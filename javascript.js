let boxTop = 200;
let boxLeft = 200;	

document.addEventListener('keydown', logKey);

function logKey(evt) {
    console.log(evt.code);
    let unicornBox = document.getElementById("box");
    if (evt.keyCode===38 || evt.keyCode===87) {
        boxTop -= 10
        unicornBox.style.top = (boxTop) + "px";}
    if (evt.keyCode===40 || evt.keyCode===83) {
        boxTop += 10
        unicornBox.style.top = (boxTop) + "px";}
    if (evt.keyCode===37 || evt.keyCode===65) {
        boxLeft -= 10
        unicornBox.style.left = (boxLeft) + "px";}
    if (evt.keyCode===39 || evt.keyCode===68) {
        boxLeft += 10
        unicornBox.style.left = (boxLeft) + "px";}   
}
